(function (module, console) {
	var express    = require('express');
	var config     = require('./configuration');
	var api        = require('src/node/routes/api/v1/');
	var bodyParser = require('body-parser');
	var initConfig = require('src/node/database/initial');
	var app        = express();

	// to support JSON-encoded bodies
	app.use(bodyParser.json());

	app.use(bodyParser.urlencoded({
		// to support URL-encoded bodies
		extended: true
	}));

	app.use(express.static('web'));

	app.use('/api/v1', api);

	app.use(function (req, res) {
		res.sendFile(__dirname + '/web/');
	});

	app.listen(config.port, function () {
		console.log('Express node.js server listening on port: ' + config.port);
	});
})(module, console);
