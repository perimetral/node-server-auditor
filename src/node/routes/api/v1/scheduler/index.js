(function (module) {
	var handler   = null;
	var express   = require('express');
	var router    = express.Router();
	var scheduler = require('src/node/com/scheduler');

	/*
	 * get all scheduled tasks
	 */
	router.get('/', function (req, res) {
		scheduler(req.sessionId).list().then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			res.json(handler.error('Error geting schedule list', err));
		});
	});

	/*
	 * add new scheduled task
	 */
	router.post('/', function (req, res) {
		var record = req.body;

		scheduler(req.sessionId).insert(record).then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			res.json(handler.error('Error adding schedule task', err));
		});
	});

	/*
	 * update scheduled task
	 */
	router.put('/:id', function (req, res) {
		var record = req.body;

		scheduler(req.sessionId).update(req.params.id, record).then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			res.json(handler.error('Error updating schedule task', err));
		});
	});

	/*
	 * delete scheduled task
	 */
	router.delete('/:id', function (req, res) {
		scheduler(req.sessionId).delete(req.params.id).then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			res.json(handler.error('schedule deletion error', err));
		});
	});

	/*
	 * enable scheduled task
	 */
	router.put('/:id/enable', function (req, res) {

		scheduler(req.sessionId).enable(req.params.id, req.sessionId).then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			// console.log('error enabled  - ', err);
			res.json(handler.error('schedule enable error', err));
		});
	});

	/*
	 * disable scheduled task
	 */
	router.put('/:id/disable', function (req, res) {
		scheduler(req.sessionId).disable(req.params.id).then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			res.json(handler.error('schedule disable error', err));
		});
	});

	module.exports = function (resHandler) {
		handler = resHandler;
		return router;
	};
})(module);
