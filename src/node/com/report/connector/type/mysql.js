(function (module) {
	var Q      = require('q');
	var util   = require('util');
	var sql    = require('mysql');
	var extend = require('extend');

	var ConnectorType = require('./');

	var MySQL = function () {
		MySQL.super_.apply(this, arguments);
	};

	util.inherits(MySQL, ConnectorType);

	MySQL.prototype.query = function (query) {
		var dfd = Q.defer(), connection, defaults;

		defaults = {
			multipleStatements: true
		};

		extend(defaults, this.config);

		connection = sql.createConnection(defaults);

		connection.query(query, function (err, rows) {
			if (err) {
				dfd.reject(err);
			} else {
				dfd.resolve(rows);
			}
		});

		connection.end();

		return dfd.promise;

	};

	module.exports = MySQL;
})(module);
