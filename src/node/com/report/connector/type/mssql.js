(function (module) {
	var Q             = require('q');
	var util          = require('util');
	var sql           = require('mssql');
	var ConnectorType = require('./');

	var MS_SQL = function () {
		MS_SQL.super_.apply(this, arguments);

		sql.on('error', function (err) {
			console.error(err);
		});
	};

	util.inherits(MS_SQL, ConnectorType);

	MS_SQL.prototype.query = function (query) {
		var dfd = Q.defer();

		sql.connect(this.config, function (err) {
			var request = new sql.Request();

			if (err) {
				dfd.reject(err);
				return;
			}

			// enable multiple recordsets in query
			request.multiple = true;

			request.query(query, function (err, recordset) {
				if (err) {
					dfd.reject(err);
				} else {
					dfd.resolve(recordset);
				}
				sql.close();
			});
		}).on('error', function () {
			console.error('socket error. Closing');
			this.close();
			dfd.reject({error: 'socket connectionn error'});
		});

		return dfd.promise;

	};

	module.exports = MS_SQL;
})(module);
