(function (module) {
	var path       = require('path');
	var appDir     = path.dirname(require.main.filename);
	var serversDir = appDir + '/servers/';
	var Data       = require('src/node/com/data');
	var validator  = require('validator');

	/**
	 * Getting configuration from chosen server
	 * @param {string} name of server
	 * @returns {object Object}
	 */
	module.exports = function (name) {
		var config = null;
		var prefs  = null;

		try {
			prefs  = require(serversDir + validator.whitelist(name, 'A-Za-z0-9_.-'));
			config = new Data.ServerConfiguration(name, prefs);
		} catch (e) {
			return;
		}

		return config;
	};
})(module);
