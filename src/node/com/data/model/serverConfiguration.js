(function (module) {
	var util = require('util');
	var Model = require('./');

	var ServerConfiguration = function (name, prefs) {
		ServerConfiguration.super_.apply(this, arguments);

		this.set('name', name);
		this.set('prefs', prefs);

	};

	util.inherits(ServerConfiguration, Model);

	module.exports = ServerConfiguration;
})(module);