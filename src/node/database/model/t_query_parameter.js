(function (module) {
	var Sequelize = require('sequelize');
	var database  = require('src/node/database')();

	var db_t_query_parameter = database.define('t_query_parameter',
		{
			// column 'id_query_parameter'
			id_query_parameter: {
				field:         "id_query_parameter",
				type:          Sequelize.INTEGER,
				allowNull:     false,
				primaryKey:    true,
				autoIncrement: true,
				comment:       "t_query_parameter id - primary key",
				validate:      {
				}
			},

			// column 'is_active'
			is_active: {
				field:     "is_active",
				type:      Sequelize.BOOLEAN,
				allowNull: false,
				comment:   "",
				defaultValue: true
			}
		},
		{
			// define the table's name
			tableName: 't_query_parameter',

			comment: 'table changed parameter for reports',

			// 'createdAt' to actually be called '_datetime_created'
			createdAt: '_datetime_created',

			// 'updatedAt' to actually be called '_datetime_updated'
			updatedAt: '_datetime_updated',

			// disable the modification of table names; By default, sequelize will automatically
			// transform all passed model names (first parameter of define) into plural.
			freezeTableName: true,

			charset: 'utf8',

			associate: function( models ) {
				db_t_query_parameter.belongsTo(
					models.t_query, {
						foreignKey: 'id_query'
					}
				);
			},

			indexes: [
/*				{
					name:   'uidx_db_t_query_parameter',
					unique: true,
					fields: [
							'id_query'
						]
				}*/
			]
		}
	);

	db_t_query_parameter.sync({ force: false });

	module.exports = db_t_query_parameter;
})(module);
