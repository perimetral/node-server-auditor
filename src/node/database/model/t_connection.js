(function (module) {
	var Sequelize        = require('sequelize');
	var default_database = require('src/node/database')();

	var TConnection = module.exports = default_database.define('t_connection',
	{
		// column 'id_connection'
		_id_connection: {
			field:         "_id_connection",
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       "t_connection id - primary key",
			validate:      {
			}
		},

		// column 'serverName'
		serverName: {
			field:     "serverName",
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   "external server name as the data source (unique)",
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		},

		// column 'is_active'
		is_active: {
			field:     "is_active",
			type:      Sequelize.BOOLEAN,
			allowNull: false,
			comment:   "",
			defaultValue: true
		}
	},
	{
		// define the table's name
		tableName: 't_connection',

		comment: "list of connections to external data sources",

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TConnection.belongsTo(
				models.t_connection_type, {
					foreignKey: 'id_connection_type'
				}
			);
		},

		indexes: [
			/*{
				name:   'uidx_t_connection_serverName',
				unique: true,
				fields: [
					'serverName'
				]
			}*/
		]
	}
	);

	module.exports = TConnection;
})(module);
