(function (module) {
	var Sequelize        = require('sequelize');
	var default_database = require('src/node/database')();

	var TReport = default_database.define('t_report',
	{
		// column 'id_report'
		id_report: {
			field:         "id_report",
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       "t_report id - primary key",
			validate:      {
			}
		},

		// column 'report_name'
		report_name: {
			field:     "report_name",
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   "report_name - name of report",
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		},

		// column 'is_active'
		is_active: {
			field:     "is_active",
			type:      Sequelize.BOOLEAN,
			allowNull: false,
			comment:   "",
			defaultValue: true
		}
	},
	{
		// define the table's name
		tableName: 't_report',

		comment: "report table",

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TReport.belongsTo(
				models.t_module, {
					foreignKey: 'id_module'
				}
			);
		},

		indexes: [
		]
	}
	);

	module.exports = TReport;
})(module);
