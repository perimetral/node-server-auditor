(function (module) {
	var Sequelize    = require('sequelize');
	var database     = require('src/node/database')();
	var Promise      = require('promise');
	var nodeSchedule = require('node-schedule');

	var t_connection_query_schedule = database.define('t_connection_query_schedule',
	{
		// column 'id'
		id: {
			field:         "id",
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       "schedule id - primary key",
			validate:      {
			}
		},

		// column 'schedule'
		schedule:{
			field:     "schedule",
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   "execution schedule, like '30 * * * * *' - execute report every minute at 30 second",
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		},

		// column 'isEnabled'
		isEnabled:{
			field:        "isEnabled",
			type:         Sequelize.BOOLEAN,

			allowNull:    false,
			defaultValue: false,
			comment:      "is execution schedule enabled",
			validate:     {
			}
		}
	},
	{
		// define the table's name
		tableName: 't_connection_query_schedule',

		comment: "list of reports execution schedules",

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			t_connection_query_schedule.belongsTo(
				models.t_query, {
					foreignKey: 'id_query'
				}
			);
		},

		indexes: [
			{
				name:   'uidx_t_connection_query_schedule',
				unique: true,
				fields: [
					'schedule'
				]
			}
		]
	}
	);

	t_connection_query_schedule.sync({ force: false }).then(function () {
		var activeSchedule, jobName,
			promises = [],
			Report = require('src/node/com/report')('start');

		t_connection_query_schedule.findAll({})
		.then( function(table) {
			var result = table.map(extractItem);

			result.forEach(function(item){
				if(!item.isEnabled){
					return;
				}

				promises.push( new Promise( function( resolve, reject ) {

				jobName = getCurrNameJob(item);

				activeSchedule = nodeSchedule.scheduleJob(jobName, item.schedule, function () {
					Report.getReportByScheduler(item.repName, item.connType, item.connName, item.moduleType, true)
						.then(resolve, reject);
					});
				}));
			});
		});
	});

	function getCurrNameJob(record) {
		return record.connType + record.connName + record.moduleType + record.repName + record.schedule;
	};

	function extractItem(item) {
		var dataValues = item.dataValues;
		var resItem    = {};

		for (var key in dataValues) {
			if (dataValues.hasOwnProperty(key)) {
				resItem[key] = dataValues[key];
			}
		}

		return resItem;
	};

	module.exports = t_connection_query_schedule;
})(module);
