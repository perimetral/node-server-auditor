(function (module) {
	var Sequelize = require('sequelize');
	var database  = require('src/node/database')();

	var db_t_connection_query_parameter = database.define('t_connection_query_parameter',
		{
			// column 'id_connection'
			id_connection: {
				field:         "id_connection",
				type:          Sequelize.INTEGER,
				allowNull:     false,
				primaryKey:    true,
				autoIncrement: true,
				comment:       "t_connection_query_parameter id - primary key",
				validate:      {
				}
			},

			// column 'value_parameter'
			value_parameter:{
				field:        "value_parameter",
				type:         Sequelize.STRING,
				allowNull:    true,
				defaultValue: "",
				comment:      "Value parameter",
				validate:     {
								}
			}
		},
		{
			// define the table's name
			tableName: 't_connection_query_parameter',

			comment: 'connection parameter for reports',

			// 'createdAt' to actually be called '_datetime_created'
			createdAt: '_datetime_created',

			// 'updatedAt' to actually be called '_datetime_updated'
			updatedAt: '_datetime_updated',

			// disable the modification of table names; By default, sequelize will automatically
			// transform all passed model names (first parameter of define) into plural.
			freezeTableName: true,

			charset: 'utf8',

			associate: function( models ) {
				db_t_connection_query_parameter.belongsTo(
					models.t_query_parameter, {
						foreignKey: 'id_query_parameter'
					}
				);
			},

			indexes: [
				{
					name:   'uidx_db_t_connection_query_parameter',
					unique: true,
					fields: [
							'value_parameter'
						]
				}
			]
		}
	);

	db_t_connection_query_parameter.sync({ force: false });

	module.exports = db_t_connection_query_parameter;
})(module);
