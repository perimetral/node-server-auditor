app.factory('sessionId', ['$window', function ($window) {
	var sessionId;
	if (!(sessionId = $window.sessionStorage.getItem('session_id'))) {
		sessionId = Math.random().toString(36).substring(3, 16) + +new Date;
		$window.sessionStorage.setItem('session_id', sessionId);
	}
	return sessionId;
}]);
