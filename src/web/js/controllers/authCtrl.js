(function (app) {
	app.controller('authCtrl', ['$scope', '$query', 'alertify', function ($scope, $query, alertify) {
			var setConf, getTypeByModuleName;

			$scope.modules = [];
			$scope.servers = [];
			$scope.dbType;
			$scope.moduleName;
			$scope.server;


			setConf = function (config) {
				$query('config/reports', config, 'PUT').then(function () {
					alertify('success', 'alertConfigSaved');
					$scope.$emit('nsaConfigUpdated', config);
				}, function () {
					alertify('error', 'alertNetworkError');
				});
			};
			
			getTypeByModuleName = function (name) {
				var i = 0;
				for(; i<$scope.modules.length; i++){
					if($scope.modules[i].name === name){
						return $scope.modules[i].type;
					}
				}
			};

			$scope.submit = function () {

				var config = {
					server: $scope.server,
					module: $scope.moduleName
				};

				setConf(config);

			};

			$scope.deleteSettings = function () {
				setConf(null);
			};

			$query('config/reports').then(function (data) {
				var res = data.response;
				var isConfigured = res.config !== null;

				$scope.modules = res.modules;
				$scope.servers = res.servers;

				if (isConfigured) {
					$scope.dbType = getTypeByModuleName(data.response.config.module);
					$scope.moduleName = data.response.config.module;
					$scope.server = data.response.config.server.name;
				} else {
					$scope.dbType = $scope.modules[0].type;
					$scope.moduleName = $scope.modules[0].name;
					$scope.server = $scope.servers[0];
					alertify('error', 'alertNotConfiguredError');
				}

			}, function () {
			});

		}]);
})(app);