(function (app) {
	app.controller('schedCreateCtrl', ['$scope', '$query', '$uibModalInstance', 'record', function ($scope, $query, $uibModalInstance, record) {
		var getTypeByModuleName;

		record = record || {};

		$scope.modules    = [];
		$scope.servers    = [];
		$scope.reports    = [];
		$scope.moduleType = null;
		$scope.connName   = null;
		$scope.connType   = null;
		$scope.repName    = null;
		$scope.schedule   = record.schedule || '';

		$scope.close = function () {
			$uibModalInstance.dismiss('close');
		};

		$scope.submit = function () {

			var str_schedule = getStrSchedule();

			var data = {
				moduleType : $scope.moduleType,
				connName   : $scope.connName,
				connType   : $scope.connType,
				repName    : $scope.repName,
				schedule   : str_schedule,
				isEnabled  : 0,
				ctEnabled  : false
			};

			if(record.id) {
				$query('schedule/' + record.id, data, 'PUT' ).then(closeWithResult, function (res) {
					$scope.mess = res.statusText;
				});
			} else {
				$query('schedule', data, 'POST' ).then(closeWithResult, function (res) {
					$scope.mess = res.statusText;
				});
			}

			function closeWithResult (result) {
				var response = null;
				if(record.id) {
					$.extend(record, data);
					$uibModalInstance.close();
				} else {
					response = result.response[0];
					response.enabled = false;

					$uibModalInstance.close(response);
				}

			};
		};

		getStrSchedule = function(){
			return ($scope.schedule === '') ? '* * * * * 1' : $scope.schedule;
		}

		getTypeByModuleName = function (name) {
			for(var i = 0; i < $scope.modules.length; i++){
				if($scope.modules[i].name === name){
					return $scope.modules[i].type;
				}
			}
		};

		replaceStructReports = function(){
			var nameModule;

			$scope.modules.forEach(function (item) {
				nameModule = item.name;

				item.reports.forEach(function (report) {
					$scope.reports.push( { module: nameModule, name: report } );
				});
			});
		}

		$query('config/reports').then(function (data) {
			var res = data.response;

			$scope.modules = res.modules;
			$scope.servers = res.servers;

			replaceStructReports();

			$scope.moduleType = record.moduleType || $scope.modules[0].type;
			$scope.connName   = record.connName   || $scope.modules[0].name;
			$scope.connType   = record.connType   || $scope.servers[0];
			$scope.repName    = record.repName    || $scope.modules[0].reports[0];

		}, function () {
		});
	}]);
})(app);
