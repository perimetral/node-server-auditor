/* global app */

/**
 * Graph Total Size of DataBase Directive
 *
 * @requires baseGraphLink
 * @requires GraphTotalSizeOfDbComp
 */
app.directive('graphTotalSizeOfDbReport', ['baseGraphLink', 'GraphTotalSizeOfDbComp', function (link, GraphTotalSizeOfDbComp) {
	return {
		restrict:    'EA',
		templateUrl: 'directives/report/graphs/default-graph.html',
		scope: {
			reportItem: '=graphTotalSizeOfDbReport'
		},
		link: function ($scope, element, attrs) {
			link($scope, element, attrs, GraphTotalSizeOfDbComp);
		}
	};
}]);
