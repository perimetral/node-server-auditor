/* global angular */

/**
 * Two Date Line Component
 *
 * @requires BaseGraphComp
 * @requires TwoDateLineCollection
 * Dependent from DateLineData Graph
 */
app.factory('twoDateLineComp', ['BaseGraphComp', 'DateLineData', 'TwoDateLineCollection', function (BaseGraphComp, DateLineData, TwoDateLineCollection) {
	var twoDateLineComp = function () {
		BaseGraphComp.call(this);

		angular.merge(this.options, {
			axes: {
				xaxis: {
					renderer: $.jqplot.DateAxisRenderer
				}
			},
			series: [
				{
					lineWidth: 2,

					markerOptions: {
						show:      false,
						style:     'square',
						lineWidth: 2
					}
				},
				{
					lineWidth: 2,

					markerOptions: {
						show:      false,
						style:     'square',
						lineWidth: 2
					}
				}
			]
		});
	};

	angular.extend(twoDateLineComp.prototype, BaseGraphComp.prototype, {
		updateReportData: function (data) {
			var graphLine = new TwoDateLineCollection(DateLineData, data);

			return graphLine.data();
		}
	});

	return twoDateLineComp;
}]);
