/**
 * Two Date Line Directive
 *
 * @requires baseGraphLink
 * @requires twoDateLineComp
 */
app.directive('twoDateLineReport', ['baseGraphLink', 'twoDateLineComp', function (link, twoDateLineComp) {
	return {
		restrict: 'EA',

		templateUrl: 'directives/report/graphs/default-graph.html',

		scope: {
			reportItem: '=twoDateLineReport'
		},

		link: function ($scope, element, attrs) {
			link($scope, element, attrs, twoDateLineComp);
		}
	};
}]);
