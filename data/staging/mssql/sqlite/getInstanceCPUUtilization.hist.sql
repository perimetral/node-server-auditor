SELECT
	 t.[ServerName]                   AS [ServerName]
	,t.[EventTime]                    AS [EventTime]
	,t.[RecordTimeStamp]              AS [RecordTimeStamp]
	,t.[RecordRingBufferType]         AS [RecordRingBufferType]
	,t.[MemoryUtilization]            AS [MemoryUtilization]
	,t.[SystemIdle]                   AS [SystemIdle]
	,t.[SQLProcessCPUUtilization]     AS [SQLProcessCPUUtilization]
	,t.[OtherProcessesCPUUtilization] AS [OtherProcessesCPUUtilization]
FROM
	[${getInstanceCPUUtilization}{0}$] t
WHERE
	t.[connectionId] = $connectionId
	AND t.[EventTime] > (
		SELECT
			DATETIME(MAX(t.[EventTime]), "-1 day")
		FROM
			[${getInstanceCPUUtilization}{0}$] t
		WHERE
			t.[connectionId] = $connectionId
	)
ORDER BY
	t.[EventTime] DESC
;
