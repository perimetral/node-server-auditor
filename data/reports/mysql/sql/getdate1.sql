SELECT
	 @@hostname   AS `ServerName`
	,'1.0'        AS `ServerVersion`
	,'sa'         AS `SUserName`
	,'sa'         AS `UserName`
	,NOW()        AS `EventTime`
	,'TestQuery1' AS `Message`
	,1            AS `RecordSet`
;

SELECT
	 @@hostname   AS `ServerName`
	,'1.0'        AS `ServerVersion`
	,'sa'         AS `SUserName`
	,'sa'         AS `UserName`
	,NOW()        AS `EventTime`
	,'TestQuery2' AS `Message`
	,2            AS `RecordSet`
;

SELECT
	 @@hostname   AS `ServerName`
	,'1.0'        AS `ServerVersion`
	,'sa'         AS `SUserName`
	,'sa'         AS `UserName`
	,NOW()        AS `EventTime`
	,'TestQuery3' AS `Message`
	,3            AS `RecordSet`
;

